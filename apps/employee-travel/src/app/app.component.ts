import { Component } from '@angular/core';

@Component({
  selector: 'ngx-live-training-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'employee-travel';
}
