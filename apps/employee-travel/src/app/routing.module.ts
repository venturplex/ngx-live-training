import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlightStatusComponent } from './flight-status/flight-status.component';
import { FlightStatusResultsComponent } from './flight-status/flight-status-results/flight-status-results.component';
import { FlightStatusDetailsComponent } from './flight-status/flight-status-details/flight-status-details.component';
import { HomeComponent } from './home/home.component';
import { FlightStatusSearchComponent } from './flight-status/flight-status-search/flight-status-search.component';
import { FlightsComponent } from './flights/flights.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component:  HomeComponent },
  { path: 'flights', component:  FlightsComponent },
  { path: 'flight-status', component: FlightStatusComponent, children: [
    { path: '',  component: FlightStatusSearchComponent },
    { path: 'results', component: FlightStatusResultsComponent },
    { path: 'details/:record', component: FlightStatusDetailsComponent },
  ]},
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [],
  exports: [RouterModule],
})
export class AppRoutingModule { }
