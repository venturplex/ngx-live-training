import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightsComponent } from './flights.component';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { FlightDetailsComponent } from './flight-details/flight-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreStateModule } from '@ngx-live-training/core-state';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FlightsComponent', () => {
  let component: FlightsComponent;
  let fixture: ComponentFixture<FlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, CoreStateModule, HttpClientTestingModule],
      declarations: [ FlightsComponent, FlightsListComponent, FlightDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
