import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Segment } from '@ngx-live-training/core-data';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-live-training-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss']
})
export class FlightDetailsComponent implements OnChanges {
  @Input() segment: Segment;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.segment && changes.segment.currentValue) {
      this.updateForm(changes.segment.currentValue);
    }
  }

  updateForm(segment: Segment) {
    this.form.patchValue({
      number: segment.number,
      departureAirport: segment.departure.airport.code,
      departureDateTime: segment.departure.date.actual,
      arrivalAirport: segment.arrival.airport.code,
      arrivalDateTime: segment.arrival.date.actual,
      aircraft: segment.aircraft,
    });
  }

  buildForm() {
    return this.fb.group({
      number: [],
      departureAirport: [],
      departureDateTime: [],
      arrivalAirport: [],
      arrivalDateTime: [],
      aircraft: []
    });
  }
}
