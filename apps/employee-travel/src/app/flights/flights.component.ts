import { Component, OnInit } from '@angular/core';
import { FlightsFacade } from '@ngx-live-training/core-state';
import { Observable } from 'rxjs';
import { Segment } from '@ngx-live-training/core-data';

@Component({
  selector: 'ngx-live-training-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {
  segments$: Observable<Segment[]> = this.flightFacade.allSegments$;
  selectedSegment: Segment;

  constructor(private flightFacade: FlightsFacade) { }

  ngOnInit() {
    this.flightFacade.loadAll();
  }

  selectSegment(segment: Segment) {
    this.selectedSegment = segment;
  }
}
