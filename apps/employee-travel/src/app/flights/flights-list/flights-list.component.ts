import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Segment } from '@ngx-live-training/core-data';

@Component({
  selector: 'ngx-live-training-flights-list',
  templateUrl: './flights-list.component.html',
  styleUrls: ['./flights-list.component.scss']
})
export class FlightsListComponent implements OnInit {
  @Input() segments: Segment[];
  @Output() selected = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
