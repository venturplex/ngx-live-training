export interface Flight {
	segments: Segment[];
}

export interface Segment {
  record: string;
	number: number;
	departure: Departure;
	arrival: Arrival;
}

export interface Departure {
	airport: Airport;
	date: Date;
	terminal: string;
	gate: string;
	baggage: string;
}

export interface Arrival {
	airport: Airport;
	date: Date;
	terminal: string;
	gate: string;
	baggage: string;
}

export interface Airport {
  city: string;
  code: string;
	state: State;
}

export interface State {
	name: string;
	abbreviation: string;
}

export interface Date {
	actual: string;
	estimated: string;
}

export const flights: Flight[] = [
  {
    segments: [
      {
        record: '31834657-17b6-4534-8d0c-c9eb9246cbc1',
        number: 3230,
        departure: {
          airport: {
            city: 'Dallas/Fort Worth',
            code: 'DFW',
            state: {
              name: 'Texas',
              abbreviation: 'TX'
            }
          },
          date: {
            actual: '2019-10-04T10:40:00',
            estimated: '2019-10-04T10:40:00'
          },
          terminal: 'T3',
          gate: 'K10',
          baggage: '4'
        },
        arrival: {
          airport: {
            city: 'Chicago',
            code: 'FWA',
            state: {
              name: 'Illinois',
              abbreviation: 'IL'
            }
          },
          date: {
            actual: '2019-10-04T14:05:00',
            estimated: '2019-10-04T14:05:00'
          },
          terminal: 'T3',
          gate: 'K10',
          baggage: '4'
        }
      }
    ]
  },
  {
    segments: [
      {
        record: '31834657-17b6-4534-8d0c-fks7vnsalf2f',
        number: 3240,
        departure: {
          airport: {
            city: 'Phoenix',
            code: 'PHX',
            state: {
              name: 'Arizona',
              abbreviation: 'AZ'
            }
          },
          date: {
            actual: '2019-11-04T02:35:00',
            estimated: '2019-11-04T02:40:00'
          },
          terminal: 'D3',
          gate: 'A10',
          baggage: '2'
        },
        arrival: {
          airport: {
            city: 'Bangor',
            code: 'BGR',
            state: {
              name: 'Maine',
              abbreviation: 'ME'
            }
          },
          date: {
            actual: '2019-12-04T12:05:00',
            estimated: '2019-12-04T12:05:00'
          },
          terminal: 'B6',
          gate: 'J2',
          baggage: '1'
        }
      }
    ]
  },
  {
    segments: [
      {
        record: '31834657-17b6-4534-8d0c-c9eb9246v4ds',
        number: 3250,
        departure: {
          airport: {
            city: 'Los Angeles',
            code: 'LAX',
            state: {
              name: 'California',
              abbreviation: 'CA'
            }
          },
          date: {
            actual: '2019-12-06T03:15:00',
            estimated: '2019-12-06T03:10:00'
          },
          terminal: 'G3',
          gate: 'F10',
          baggage: '12'
        },
        arrival: {
          airport: {
            city: 'Salt Lake City',
            code: 'SLC',
            state: {
              name: 'Utah',
              abbreviation: 'UT'
            }
          },
          date: {
            actual: '2019-12-06T14:05:00',
            estimated: '2019-12-06T14:05:00'
          },
          terminal: 'C2',
          gate: 'D3',
          baggage: '14'
        }
      }
    ]
  }
]
