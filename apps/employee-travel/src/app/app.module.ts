import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreStateModule } from '@ngx-live-training/core-state';
import { UiToolbarModule } from '@ngx-live-training/ui-toolbar';
import { AppComponent } from './app.component';
import { FlightStatusDetailsComponent } from './flight-status/flight-status-details/flight-status-details.component';
import { FlightStatusResultsComponent } from './flight-status/flight-status-results/flight-status-results.component';
import { FlightStatusSearchComponent } from './flight-status/flight-status-search/flight-status-search.component';
import { FlightStatusComponent } from './flight-status/flight-status.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './routing.module';
import { FlightsComponent } from './flights/flights.component';
import { FlightDetailsComponent } from './flights/flight-details/flight-details.component';
import { FlightsListComponent } from './flights/flights-list/flights-list.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightStatusComponent,
    FlightStatusResultsComponent,
    FlightStatusDetailsComponent,
    FlightStatusSearchComponent,
    HomeComponent,
    FlightsComponent,
    FlightDetailsComponent,
    FlightsListComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    UiToolbarModule,
    HttpClientModule,
    CoreStateModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
