import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FlightStatusResultsComponent } from './flight-status-results.component';
import { Flight } from '@ngx-live-training/core-data';
import { of } from 'rxjs';
import { FlightsFacade } from '@ngx-live-training/core-state';

const mockFlight: Flight = {
  "segments": [
    {
      "aircraft": 768,
      "record": "31834657-17b6-4534-8d0c-c9eb9246cbc1",
      "number": 3230,
      "departure": {
        "airport": {
          "city": "Dallas/Fort Worth",
          "code": "DFW",
          "state": {
            "name": "Texas",
            "abbreviation": "TX"
          }
        },
        "date": {
          "actual": "2019-10-04T10:40:00",
          "estimated": "2019-10-04T10:40:00"
        },
        "terminal": "T3",
        "gate": "K10",
        "baggage": "4"
      },
      "arrival": {
        "airport": {
          "city": "Chicago",
          "code": "FWA",
          "state": {
            "name": "Illinois",
            "abbreviation": "IL"
          }
        },
        "date": {
          "actual": "2019-10-04T14:05:00",
          "estimated": "2019-10-04T14:05:00"
        },
        "terminal": "T3",
        "gate": "K10",
        "baggage": "4"
      }
    }
  ]
}

class MockFlightFacade {
  allFlights$ = of([mockFlight]);

  loadAll() {
    return jest.fn();
  }
}

describe('FlightStatusListComponent', () => {
  let component: FlightStatusResultsComponent;
  let fixture: ComponentFixture<FlightStatusResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ FlightStatusResultsComponent ],
      providers: [{ provide: FlightsFacade, useClass: MockFlightFacade }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightStatusResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
