import { Component, OnInit } from '@angular/core';
import { FlightService, Flight, Segment } from '@ngx-live-training/core-data';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FlightsFacade } from '@ngx-live-training/core-state';

@Component({
  selector: 'ngx-live-training-flight-results-list',
  templateUrl: './flight-status-results.component.html',
  styleUrls: ['./flight-status-results.component.scss']
})
export class FlightStatusResultsComponent implements OnInit {
  flights$: Observable<Flight[]> = this.flightFacade.allFlights$;

  constructor(private flightFacade: FlightsFacade) { }

  ngOnInit() {
    this.flightFacade.loadAll();
  }

  getFlightHeading(flight: Flight): string {
    if (flight && flight.segments && flight.segments.length > 0) {
      const departureAirport = flight.segments[0].departure.airport;
      const arrivalAirport = flight.segments[flight.segments.length - 1].arrival.airport;
      return `${departureAirport.city}, ${departureAirport.state.abbreviation} to ${arrivalAirport.city}, ${arrivalAirport.state.abbreviation}`;
    }
    return '';
  }
  
  getFlightTime(flight: Flight): string {
    return flight && flight.segments && flight.segments.length > 0 ? `${new Date(flight.segments[0].departure.date.estimated).toUTCString()}` : '';
  }
}
