import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

enum SearchTypes {
  AIRPORT = 'airport',
  FLIGHT_NUMBER = 'flight number'
}

@Component({
  selector: 'ngx-live-training-flight-status-search',
  templateUrl: './flight-status-search.component.html',
  styleUrls: ['./flight-status-search.component.scss']
})
export class FlightStatusSearchComponent implements OnInit {
  searchType = SearchTypes.AIRPORT;
  SearchTypes = SearchTypes;
  dates = this.generateDates();

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  selectSearchType(searchType: SearchTypes) {
    this.searchType = searchType;
  }

  isAirportSearchType(searchType: SearchTypes) {
    return searchType === SearchTypes.AIRPORT;
  }

  searchFlights() {
    this.router.navigate(['results'], {relativeTo: this.route});
  }

  generateDates() {
    const dates = [];
    for (let i = 0; i < 3; i++) {
      const date = new Date();
      date.setDate(date.getDate() + i);
      dates.push(date);
    }
    return dates;
  }
}
