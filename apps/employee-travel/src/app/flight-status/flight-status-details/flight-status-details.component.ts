import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap, tap, withLatestFrom, map } from 'rxjs/operators';
import { FlightService, Flight, Segment } from '@ngx-live-training/core-data';
import { Observable } from 'rxjs';
import { FlightsFacade } from '@ngx-live-training/core-state';

@Component({
  selector: 'ngx-live-training-flight-status-details',
  templateUrl: './flight-status-details.component.html',
  styleUrls: ['./flight-status-details.component.scss']
})
export class FlightStatusDetailsComponent implements OnInit {
  segments$: Observable<Segment[]> = this.flightFacade.allSegments$.pipe(
    withLatestFrom(this.route.params),
    map(([segments, params]) => segments.filter(segment => segment.record === params.record))
  );

  constructor(private route: ActivatedRoute, private flightFacade: FlightsFacade) { }

  ngOnInit() {
    this.route.params
      .pipe(tap((params: Params) => this.flightFacade.loadByRecord(params.record)))
      .subscribe();
  }

  getFlightHeading(segments: Segment[]): string {
    if (segments && segments.length > 0) {
      const departureAirport = segments[0].departure.airport;
      const arrivalAirport = segments[segments.length - 1].arrival.airport;
      return `${departureAirport.city}, ${departureAirport.state.abbreviation} to ${arrivalAirport.city}, ${arrivalAirport.state.abbreviation}`;
    }
    return '';
  }
  
  getFlightTime(segments: Segment[]): string {
    return segments && segments.length > 0 ? `${new Date(segments[0].departure.date.estimated).toUTCString()}` : '';
  }
}
