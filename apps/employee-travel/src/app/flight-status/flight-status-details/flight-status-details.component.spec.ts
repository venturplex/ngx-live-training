import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FlightStatusDetailsComponent } from './flight-status-details.component';
import { FlightsFacade } from '@ngx-live-training/core-state';
import { of } from 'rxjs';
import { Segment } from '@ngx-live-training/core-data';

const mockSegment: Segment = {
  "aircraft": 738,
  "record": "31834657-17b6-4534-8d0c-c9eb9246cbc1",
  "number": 3230,
  "departure": {
    "airport": {
      "city": "Dallas/Fort Worth",
      "code": "DFW",
      "state": {
        "name": "Texas",
        "abbreviation": "TX"
      }
    },
    "date": {
      "actual": "2019-10-04T10:40:00",
      "estimated": "2019-10-04T10:40:00"
    },
    "terminal": "T3",
    "gate": "K10",
    "baggage": "4"
  },
  "arrival": {
    "airport": {
      "city": "Chicago",
      "code": "FWA",
      "state": {
        "name": "Illinois",
        "abbreviation": "IL"
      }
    },
    "date": {
      "actual": "2019-10-04T14:05:00",
      "estimated": "2019-10-04T14:05:00"
    },
    "terminal": "T3",
    "gate": "K10",
    "baggage": "4"
  }
}

class MockFlightFacade {
  allSegments$ = of([mockSegment]);

  loadByRecord() {
    return jest.fn();
  }
}

describe('FlightStatusDetailsComponent', () => {
  let component: FlightStatusDetailsComponent;
  let fixture: ComponentFixture<FlightStatusDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightStatusDetailsComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        { provide: FlightsFacade, useClass: MockFlightFacade}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightStatusDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
