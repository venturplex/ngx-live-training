module.exports = {
  name: 'employee-travel',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/employee-travel',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
