import { getGreeting } from '../support/app.po';

const data = require('../../../../api/db.json');

const moment = require('moment');

describe('employee-travel', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to Travel Planner!');
  });


  describe('toolbar', () => {
    it('clicking "ADMIN" link in toolbar should navigate to "flights"', () => {
      cy.visit('/');
      cy.get('header').contains('ADMIN').click();
      cy.url().should('include', '/flights');
    });

    it('clicking app logo in toolbar should navigate to "home"', () => {
      cy.visit('/');
      cy.get('header').contains('Travel Planner').click();
      cy.url().should('include', '/home');
    });
  });

  describe('home', () => {
    it('root should navigate to home', () => {
      cy.visit('/');
      cy.url().should('include', '/home');
      cy.get('h1').contains('Welcome to Travel Planner!');
    });
  });

  describe('flights', () => {
    const flight = data.flights[0].segments[0];
    const departureTime = moment(flight.departure.date.actual).format('h:mm A');
    const arrivalTime = moment(flight.arrival.date.actual).format('h:mm A');

    beforeEach(() => {
      cy.visit('/');
      cy.get('.callout a').click();
    });

    it('should navigate', () => {
      cy.url().should('include', '/flights');
      cy.get('h1').should('contain', 'Flights');
    });

    it('should have data', () => {
      cy.get('th:first-child').contains('Flight');
      cy.get('th:nth-child(2)').contains('Depart');
      cy.get('th:nth-child(3)').contains('Arrive');
      cy.get('th:last-child').contains('Aircraft');

      const firstDataRow = cy.get('tbody tr:first-child');
      firstDataRow.get('td:first-child').contains(flight.number);
      firstDataRow.get('td:nth-child(2)').contains(flight.departure.airport.code).contains(departureTime);
      firstDataRow.get('td:nth-child(3)').contains(flight.arrival.airport.code).contains(arrivalTime);
      firstDataRow.get('td:last-child').contains(flight.aircraft);
    });

    it('should should select a flight', () => {
      cy.get('tbody tr:first-child').click();
      cy.get('input#number').should('have.value', flight.number.toString());
      cy.get('input#aircraft').should('have.value', flight.aircraft.toString());
      cy.get('input#departure-airport').should('have.value', flight.departure.airport.code);
      cy.get('input#arrival-airport').should('have.value', flight.arrival.airport.code);
      cy.get('input#departure-date-time').should('have.value', flight.departure.date.actual);
      cy.get('input#arrival-date-time').should('have.value', flight.arrival.date.actual);
    });
  });
});
