## app.component.html
```html
<ngx-live-training-toolbar></ngx-live-training-toolbar>
<div class="margin-top-1 display-block"> </div>
```

## home.component.html 
```html
<div class="callout primary">
  <h1>Welcome to Travel Planner!</h1>
  <p>To take a tour, head on over to <a href="flight-status">Flight Status</a>.</p>
</div>
```

## flight-status.component.html
```html
<div class="grid-container">
  <h1>Flight status</h1>
  <div class="margin-vertical-1">
    <button class="clear button padding-horizontal-0"><< Back</button>
  </div>
</div>
```

## flight-status-details.component.html
```html
<hr>
<ul class="menu icons icon-top flex-container align-center">
  <li><a><i class="fi-list-thumbnails"></i> <span>Priority list</span></a></li>
  <li><div class="action-divider margin-horizontal-2"></div></li>
  <li><a><i class="fi-comment-quotes"></i> <span>Create notification</span></a></li>
</ul>
<hr>
<h4 class="flight-route-heading"> </h4>
<h6 class="flight-date-heading"> </h6>
<table class="unstriped">
  <thead>
    <tr>
      <th width="150">Flight</th>
      <th width="150">Depart</th>
      <th width="150">Arrive</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <img width="15" src="assets/aa-icon.png" alt="AA Logo">

      </td>
      <td>
        <div><strong> <br> </strong></div>
        <div><small>Actual: </small></div>
        <div><small>Estimated: </small></div>
        <div><small>Terminal: </small></div>
        <div><small>Gate: </small></div>
      </td>
      <td>
        <div><strong> <br> </strong></div>
        <div><small>Actual: </small></div>
        <div><small>Estimated: </small></div>
        <div><small>Terminal: </small></div>
        <div><small>Gate: </small></div>
        <div><small>Baggage: </small></div>
      </td>
    </tr>
  </tbody>
</table>
```

## flight-status-results.component.html
```html
<div class="grid-container">
  <h4 class="flight-route-heading"> </h4>
  <h6 class="flight-date-heading"> </h6>

  <table class="unstriped">
    <thead>
      <tr>
        <th width="150">Flight</th>
        <th width="150">Depart</th>
        <th width="150">Arrive</th>
        <th width="150"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <img width="15" src="assets/aa-icon.png" alt="AA Logo">
          {{segment.number}}
        </td>
        <td><strong> <br> </strong></td>
        <td><strong> <br> </strong></td>
        <td><a>Check status >></a></td>
      </tr>
    </tbody>
  </table>
</div>
```

## flight-status-search.component.html
```html
<div class="grid-container search-by-container">
  <div class="flex-container align-baseline margin-vertical-1">
    <div class="flex-child-shrink padding-right-1">Search by:</div>
    <div class="flex-child-auto small button-group">
      <button
        class="button margin-right-1">Airport</button>
      <button
        class="button hollow">Flight Number</button>
    </div>
  </div>
</div>
<form>
  <div class="grid-container fluid">
    <div class="margin-bottom-1">(<span class="required-indicator"></span> Required)</div>
    <div class="grid-x grid-padding-x">
      <div class="small-3 cell">
        <label>From <span class="required-indicator"></span>
          <input type="text" placeholder="City or airport">
        </label>
      </div>
      <div class="small-3 cell">
        <label>To <span class="required-indicator"></span>
          <input type="text" placeholder="City or airport">
        </label>
      </div>
      <div class="small-3 cell">
        <label>Flight number <span class="required-indicator"></span>
          <input type="text" placeholder="Flight number">
        </label>
      </div>
      <div class="small-3 cell">
        <label>Depart
          <select name="depart" id="depart">
            <option> </option>
          </select>
        </label>
      </div>
      <div class="small-3 cell flex-container align-bottom">
        <button type="submit" class="button width-100">Search</button>
      </div>
    </div>
  </div>
</form>
```

## toolbar.component.html
```html
<header class="top-bar ui-toolbar">
  <div class="top-bar-left">
    <ul class="menu">
      <li class="menu-text"><strong>Travel</strong> Planner</li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li><a href="#">Find flights</a></li>
      <li><a href="#">Trips</a></li>
      <li><a href="#">Travelers</a></li>
      <li><a href="#">Tools</a></li>
    </ul>
  </div>
</header>
```
