## Generate NX repo

`npm init nx-workspace ngx-live-training`
- What to create in the new workspace: Angular
- Application Name: employee-travel
- Default stylesheet format: SASS(.scss)

## Generate `employee-travel` components

`ng g c home --project=employee-travel`

`ng g c flight-status --project=employee-travel`
`ng g c flight-status/flight-status-details --project=employee-travel`
`ng g c flight-status/flight-status-results --project=employee-travel`
`ng g c flight-status/flight-status-search --project=employee-travel`

## Generate Libs
`nx g lib core-data`
- Which stylesheet format would you like to use: SASS(.scss)

`nx g lib core-state`
- Which stylesheet format would you like to use: SASS(.scss)

`nx g lib ui-toolbar`
- Which stylesheet format would you like to use: SASS(.scss)

## Generate libs/ui-toolbar `toolbar` component
`ng g c toolbar --project=ui-toolbar`

## Generate libs/core-data `flight` service
`ng g s flight --project=core-data --flat`

## Generate libs/core-state `flights` feature state
`ng g @nrwl/angular:ngrx flights --module=libs/core-state/src/lib/core-state.module.ts --directory=flights --syntax=creators --facade`
- Is this the root state of the application: No

