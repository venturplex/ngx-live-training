import * as fromFlights from './flights/flights.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
  [fromFlights.FLIGHTS_FEATURE_KEY]: fromFlights.FlightsState,
}

export const reducers: ActionReducerMap<AppState> = {
  [fromFlights.FLIGHTS_FEATURE_KEY]: fromFlights.reducer,
};
