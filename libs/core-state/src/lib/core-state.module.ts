import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '@env/environment';
import { reducers } from '.';

import { FlightsEffects } from './flights/flights.effects';
import { FlightsFacade } from './flights/flights.facade';
import { NxModule } from '@nrwl/angular';

@NgModule({
  imports: [
    NxModule.forRoot(),
    CommonModule,
    StoreModule.forRoot(
      reducers,
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true
        }
      }
    ),
    EffectsModule.forRoot([FlightsEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [FlightsFacade]
})
export class CoreStateModule {}
