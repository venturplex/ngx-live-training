import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import * as fromFlights from './flights.reducer';
import * as FlightsSelectors from './flights.selectors';
import * as FlightsActions from './flights.actions';

@Injectable()
export class FlightsFacade {
  loaded$ = this.store.pipe(select(FlightsSelectors.getFlightsLoaded));
  allFlights$ = this.store.pipe(select(FlightsSelectors.getAllFlights));
  allSegments$ = this.store.pipe(select(FlightsSelectors.getAllSegments));
  selectedFlights$ = this.store.pipe(select(FlightsSelectors.getSelected));

  constructor(private store: Store<fromFlights.FlightsPartialState>) {}

  loadAll() {
    this.store.dispatch(FlightsActions.loadFlights());
  }

  loadByRecord(record: string) {
    this.store.dispatch(FlightsActions.loadFlightByRecord({record}));
  }
}
