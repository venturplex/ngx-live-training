import { createAction, props } from '@ngrx/store';
import { Flight } from '@ngx-live-training/core-data';

export const loadFlights = createAction('[Flights] Load Flights');

export const loadFlightByRecord = createAction(
  '[Flights] Load Flight',
  props<{ record: string }>()
);

export const loadFlightsSuccess = createAction(
  '[Flights] Load Flights Success',
  props<{ flights: Flight[] }>()
);

export const loadFlightsFailure = createAction(
  '[Flights] Load Flights Failure',
  props<{ error: any }>()
);
