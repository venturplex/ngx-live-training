import { FlightsState, flightsAdapter, initialState } from './flights.reducer';
import * as FlightsSelectors from './flights.selectors';
import { Flight } from '@ngx-live-training/core-data';

describe('Flights Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getFlightsId = it => it.segments[0].record;
  const createFlight = (id: string, name = '') =>
    ({
      segments: [
        {
          record: id
        }
      ]
    } as Flight);

  let state;

  beforeEach(() => {
    state = {
      flights: flightsAdapter.addAll(
        [
          createFlight('PRODUCT-AAA'),
          createFlight('PRODUCT-BBB'),
          createFlight('PRODUCT-CCC')
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true
        }
      )
    };
  });

  describe('Flights Selectors', () => {
    it('getAllFlights() should return the list of Flights', () => {
      const results = FlightsSelectors.getAllFlights(state);
      const selId = getFlightsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = FlightsSelectors.getSelected(state);
      const selId = getFlightsId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getFlightsLoaded() should return the current 'loaded' status", () => {
      const result = FlightsSelectors.getFlightsLoaded(state);

      expect(result).toBe(true);
    });

    it("getFlightsError() should return the current 'error' state", () => {
      const result = FlightsSelectors.getFlightsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
