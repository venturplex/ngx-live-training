import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as FlightsActions from './flights.actions';
import { Flight } from '@ngx-live-training/core-data';

export const FLIGHTS_FEATURE_KEY = 'flights';

export interface FlightsState extends EntityState<Flight> {
  selectedId?: string | number; // which Flights record has been selected
  loaded: boolean; // has the Flights list been loaded
  error?: string | null; // last none error (if any)
}

export interface FlightsPartialState {
  readonly [FLIGHTS_FEATURE_KEY]: FlightsState;
}

export const flightsAdapter: EntityAdapter<Flight> = createEntityAdapter<
  Flight
>({selectId: (flight) => flight.segments[0].record});

export const initialState: FlightsState = flightsAdapter.getInitialState({
  // set initial required properties
  loaded: false
});

const flightsReducer = createReducer(
  initialState,
  on(FlightsActions.loadFlights, state => ({
    ...state,
    loaded: false,
    error: null
  })),
  on(FlightsActions.loadFlightsSuccess, (state, { flights }) =>
    flightsAdapter.addAll(flights, { ...state, loaded: true })
  ),
  on(FlightsActions.loadFlightsFailure, (state, { error }) => ({
    ...state,
    error,
    loaded: true
  }))
);

export function reducer(state: FlightsState | undefined, action: Action) {
  return flightsReducer(state, action);
}
