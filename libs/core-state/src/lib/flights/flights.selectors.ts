import { flatMap } from 'lodash';

import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  FLIGHTS_FEATURE_KEY,
  FlightsState,
  FlightsPartialState,
  flightsAdapter
} from './flights.reducer';

// Lookup the 'Flights' feature state managed by NgRx
export const getFlightsState = createFeatureSelector<
  FlightsPartialState,
  FlightsState
>(FLIGHTS_FEATURE_KEY);

const { selectAll, selectEntities } = flightsAdapter.getSelectors();

export const getFlightsLoaded = createSelector(
  getFlightsState,
  (state: FlightsState) => state.loaded
);

export const getFlightsError = createSelector(
  getFlightsState,
  (state: FlightsState) => state.error
);

export const getAllFlights = createSelector(
  getFlightsState,
  (state: FlightsState) => selectAll(state)
);

export const getFlightsEntities = createSelector(
  getFlightsState,
  (state: FlightsState) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getFlightsState,
  (state: FlightsState) => state.selectedId
);

export const getSelected = createSelector(
  getFlightsEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);

export const getAllSegments = createSelector(
  getAllFlights,
  flights => flatMap(flights, entity => entity.segments)
);
