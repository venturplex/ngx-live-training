import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';
import { Observable, of } from 'rxjs';
import * as FlightsActions from './flights.actions';
import { FlightsEffects } from './flights.effects';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlightService } from '@ngx-live-training/core-data';


describe('FlightsEffects', () => {
  let actions: Observable<any>;
  let effects: FlightsEffects;
  let service: FlightService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot(), HttpClientTestingModule],
      providers: [
        FlightsEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore()
      ]
    });

    service = TestBed.get(FlightService);
    effects = TestBed.get(FlightsEffects);
  });

  describe('loadFlights$', () => {
    it('should work', () => {
      jest.spyOn(service, 'all').mockReturnValue(of([]));
      actions = hot('-a-', { a: FlightsActions.loadFlights() });

      const expected = hot('-a-', {
        a: FlightsActions.loadFlightsSuccess({ flights: [] })
      });

      expect(effects.loadFlights$).toBeObservable(expected);
    });
  });
});
