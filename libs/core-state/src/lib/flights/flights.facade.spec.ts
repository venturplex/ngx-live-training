import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/angular/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/angular';

import { FlightsEffects } from './flights.effects';
import { FlightsFacade } from './flights.facade';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import * as FlightsSelectors from './flights.selectors';
import * as FlightsActions from './flights.actions';
import {
  FLIGHTS_FEATURE_KEY,
  FlightsState,
  initialState,
  reducer
} from './flights.reducer';
import { Flight, FlightService } from '@ngx-live-training/core-data';
import { of } from 'rxjs';

interface TestSchema {
  flights: FlightsState;
}

const mockDestination =  {
  airport: {
    code: '',
    city: '',
    state: {
      name: '',
      abbreviation: '',
    },
  },
  date: {
    actual: '',
    estimated: '',
  },
  terminal: '',
  gate: '',
  baggage: '',
};

const mockFlight: Flight = {
  segments: [
    {
      record: '',
      aircraft: 738,
      number: 12345,
      departure: mockDestination,
      arrival: mockDestination
    }
  ]
}

describe('FlightsFacade', () => {
  let facade: FlightsFacade;
  let store: Store<TestSchema>;
  let service: FlightService;

  beforeEach(() => {});

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          HttpClientTestingModule,
          StoreModule.forFeature(FLIGHTS_FEATURE_KEY, reducer),
          EffectsModule.forFeature([FlightsEffects])
        ],
        providers: [FlightsFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      service = TestBed.get(FlightService);
      facade = TestBed.get(FlightsFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async done => {
      try {
        jest.spyOn(service, 'all').mockReturnValue(of([]));
        let list = await readFirst(facade.allFlights$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.loadAll();

        list = await readFirst(facade.allFlights$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `loadFlightsSuccess` to manually update list
     */
    it('allFlights$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allFlights$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(
          FlightsActions.loadFlightsSuccess({
            flights: [ mockFlight ]
          })
        );

        list = await readFirst(facade.allFlights$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(1);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
