import { Injectable } from '@angular/core';
import { createEffect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';

import { FlightService } from '@ngx-live-training/core-data';

import { FlightsPartialState } from './flights.reducer';
import * as FlightsActions from './flights.actions';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class FlightsEffects {
  loadFlights$ = createEffect(() =>
    this.dataPersistence.fetch(FlightsActions.loadFlights, {
      run: (
        action: ReturnType<typeof FlightsActions.loadFlights>,
        state: FlightsPartialState
      ) => {
        return this.flightService.all()
          .pipe(map(flights => FlightsActions.loadFlightsSuccess({ flights })))
      },

      onError: (
        action: ReturnType<typeof FlightsActions.loadFlights>,
        error
      ) => {
        console.error('Error', error);
        return FlightsActions.loadFlightsFailure({ error });
      }
    }),
  );

  loadFlightsByRecord$ = createEffect(() =>
    this.dataPersistence.fetch(FlightsActions.loadFlightByRecord, {
      run: (
        action: ReturnType<typeof FlightsActions.loadFlightByRecord>,
        state: FlightsPartialState
      ) => {
        return this.flightService.getByRecord(action.record)
          .pipe(map(flights => FlightsActions.loadFlightsSuccess({ flights })))
      },

      onError: (
        action: ReturnType<typeof FlightsActions.loadFlightByRecord>,
        error
      ) => {
        console.error('Error', error);
        return FlightsActions.loadFlightsFailure({ error });
      }
    }),
  );

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<FlightsPartialState>,
    private flightService: FlightService
  ) {}
}
