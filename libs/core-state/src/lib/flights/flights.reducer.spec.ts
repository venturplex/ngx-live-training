import * as FlightsActions from './flights.actions';
import { FlightsState, initialState, reducer } from './flights.reducer';
import { Flight } from '@ngx-live-training/core-data';

describe('Flights Reducer', () => {
  const createFlight = (id: string, name = '') =>
    ({
      segments: [
        {
          record: id
        }
      ]
    } as Flight);

  beforeEach(() => {});

  describe('valid Flights actions', () => {
    it('loadFlightsSuccess should return set the list of known Flights', () => {
      const flights = [
        createFlight('PRODUCT-AAA'),
        createFlight('PRODUCT-zzz')
      ];
      const action = FlightsActions.loadFlightsSuccess({ flights });

      const result: FlightsState = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
