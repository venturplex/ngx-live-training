import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Flight, Segment } from './flight';
const ENDPOINT = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class FlightService {
  constructor(private http: HttpClient) { }

  all(): Observable<Flight[]> {
    return this.http.get<Flight[]>(`${ENDPOINT}/flights`);
  }

  getByRecord(record: string): Observable<Flight[]> {
    return this.http.get<Flight[]>(`${ENDPOINT}/flights?record=${record}`);
  }
}
