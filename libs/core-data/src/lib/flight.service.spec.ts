import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FlightService } from './flight.service';

describe('FlightService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: FlightService = TestBed.get(FlightService);
    expect(service).toBeTruthy();
  });
});
