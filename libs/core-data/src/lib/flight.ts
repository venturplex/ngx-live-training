
export interface Departure {
	airport: Airport;
	date: Date;
	terminal: string;
	gate: string;
	baggage: string;
}

export interface State {
	name: string;
	abbreviation: string;
}

export interface Airport {
  city: string;
  code: string;
	state: State;
}

export interface Date {
	actual: string;
	estimated: string;
}

export interface Arrival {
	airport: Airport;
	date: Date;
	terminal: string;
	gate: string;
	baggage: string;
}

export interface Segment {
  aircraft: number;
  record: string;
	number: number;
	departure: Departure;
	arrival: Arrival;
}

export interface Flight {
	segments: Segment[];
}
